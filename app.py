import flask
from flask import request, render_template
import json
import openai
import speech_recognition as sr

# Initialize recognizer class (for recognizing the speech)

openai.api_key = "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"
audio_recogniser = sr.Recognizer()

# Create Flask application
app = flask.Flask(__name__)


def get_audio_input():
    with sr.Microphone() as source:
        print("Talk, you have 10 seconds")
        audio_text = audio_recogniser.listen(source, timeout=5, phrase_time_limit=10)
        print("Time over, thanks")
        # recoginize_() method will throw a request error if the API is unreachable, hence using exception handling

        try:
            # using google speech recognition
            audio_text_op = audio_recogniser.recognize_google(audio_text)
            print("Text: " + audio_text_op)
            return audio_text_op
        except:
            print("Sorry, I did not get that")


def get_completion_from_messages(messages, model="gpt-3.5-turbo", temperature=0):
    response = openai.ChatCompletion.create(
        model=model,
        messages=messages,
        temperature=temperature, # this is the degree of randomness of the model's output
    )
#     print(str(response.choices[0].message))
    return response.choices[0].message["content"]


messages = []


@app.route('/audio', methods=['POST'])
def input_audio():
    audio_text = get_audio_input()
    messages.append({'role': 'user', 'content': audio_text})
    response = get_completion_from_messages(messages, temperature=0)
    messages.append({'role': 'assistant', 'content': response})
    final_ret = {"status": "Success", "message": response}
    return final_ret


@app.route('/text', methods=['POST'])
def input_text():
    input_text = json.dumps(request.data.decode)('utf-8')['question']
    messages.append({'role': 'user', 'content': input_text})
    response = get_completion_from_messages(messages, temperature=0)
    messages.append({'role': 'assistant', 'content': response})
    final_ret = {"status": "Success", "message": response}
    return final_ret




# Run the application
@app.route('/', methods=['GET'])
def hello():
    return 'Hello, World!'


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=6000)
